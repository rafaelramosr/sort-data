# SORT DATA
Sort an array of photos and users in text files.

## REQUISITOS
1. Crear dos archivos de textos en blanco (`users.txt` y `photos.txt`) dentro de una carpeta `results`.
2. Ordenar los datos del archivo `photos.json` en orden alfabético de los títulos e imprimirlos en el archivo `photos.txt`.
3. Ordenar los datos del archivo `users.json` en orden cronológico de las fechas de nacimientos e imprimirlos en el archivo `users.txt`.
4. El archivo `photos.txt` debe verse de la siguiente manera: 
```txt
Foto 1
Título: error quasi sunt cupiditate voluptate ea odit beatae
URL: https://via.placeholder.com/600/6dd9cb

Foto 2
Título: faltal quasi sunt cupiditate voluptate ea odit beatae
URL: https://via.placeholder.com/600/6dd9cb

Foto n
...
```

5. El archivo `users.txt` debe verse de la siguiente manera: 
```txt
Usuario 1
Nombre: Monsieur Aurélien Fontai
Género: Másculino
Dirección: Genève, Epesses, 2443 place de la mairie
Fecha de nacimiento: Thu Jan 08 1970

Usuario 2
Nombre: Monsieur Aurélien Fontai
Género: Másculino
Dirección: Genève, Epesses, 2443 place de la mairie
Fecha de nacimiento: Thu Jan 08 1970

Usuario n
...
```

6. Se debe poder ejecutar el paso 1 con el comando `npm run build`
7. Se debe poder ejecutar el paso 2 y 3 con el comando `npm run start`
